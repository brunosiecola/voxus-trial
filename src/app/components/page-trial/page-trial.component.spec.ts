import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageTrialComponent } from './page-trial.component';

describe('PageTrialComponent', () => {
  let component: PageTrialComponent;
  let fixture: ComponentFixture<PageTrialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageTrialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageTrialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
