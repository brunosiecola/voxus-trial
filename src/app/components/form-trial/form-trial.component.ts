import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-form-trial',
  templateUrl: './form-trial.component.html',
  styleUrls: ['./form-trial.component.sass']
})
export class FormTrialComponent implements OnInit {

  sent: boolean;
  myForm: FormGroup;

  constructor(formBuilder: FormBuilder) {
    this.sent = false;
    this.myForm = formBuilder.group({
      nomeCompleto: [null, Validators.required],
      pais: [null, Validators.required],
      telefone: [null, Validators.required],
      email: [null, Validators.compose([ Validators.required, Validators.email ])],
      nomeEmpresa: [null, Validators.required],
      urlSite: [null, Validators.required],
      verbaMensalMidia: [null, Validators.required]
    });
  }

  ngOnInit() {
  }

  getValidationErrorMessage(formControlName: string): string {
    const formControl = this.myForm.controls[formControlName];
    if(formControl.invalid) {
      if(formControl.hasError('required'))
        return 'Informe esse campo.';
      else if(formControl.hasError('email'))
        return 'Informe um endereço de e-mail válido.';
      else
        return '';
    }
  }

  onSubmit(body: any): void {
    this.sent = true;
  }

}
